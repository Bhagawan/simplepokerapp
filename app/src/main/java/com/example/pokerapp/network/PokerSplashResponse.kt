package com.example.pokerapp.network

import androidx.annotation.Keep

@Keep
data class PokerSplashResponse(val url : String)