package com.example.pokerapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.pokerapp.databinding.ActivityMainBinding
import com.example.pokerapp.viewmodel.MainViewModel
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]

        setContentView(binding.root)
    }

    override fun onResume() {
        lifecycleScope.launch {
            viewModel.gameFlow.collect {
                binding.game.loadState(it)
            }
        }
        super.onResume()
    }

    override fun onPause() {
        viewModel.saveState(binding.game.saveState())
        super.onPause()
    }
}