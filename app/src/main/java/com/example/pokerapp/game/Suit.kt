package com.example.pokerapp.game

enum class Suit(val n : Byte) {
    HEARTS(2), DIAMONDS(1), CLUBS(0), SPADES(3)
}