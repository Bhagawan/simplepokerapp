package com.example.pokerapp.game

enum class PlayerAction {
    WAITING, CHECK, CALL, RAISE, FOLD, ALL_IN, LOST
}