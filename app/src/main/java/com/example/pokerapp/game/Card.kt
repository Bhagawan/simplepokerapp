package com.example.pokerapp.game

import android.graphics.Bitmap

data class Card(val suit :Suit, var value: Byte, val bitmap: Bitmap) {

    override fun equals(other: Any?): Boolean {
        return if(other is Card) (suit == other.suit) && (value == other.value)
        else false
    }

    override fun hashCode(): Int {
        var result = suit.hashCode()
        result = 31 * result + value
        result = 31 * result + bitmap.hashCode()
        return result
    }
}