package com.example.pokerapp.game

class Hand(cards : List<Card>) {
    var straight : List<Card>? = null
    var straightFlush = false
    var flush : List<Card>? = null
    var four : Byte? = null
    var triple : Byte? = null
    var pairs = emptyList<Byte>()
    var everythingElse = emptyList<Card>()

    companion object {
        val handComparator = Comparator<Hand> { o1, o2 -> o1.compareTo(o2) }
    }


    init {
        if(cards.size > 1) {
            val c : ArrayList<Card> = ArrayList(cards.sortedBy { it.value })
            val ace = if(c[0].value == 0.toByte()) c[0] else null
            val s = arrayListOf(c[0])

            for(n in 1 until c.size) {
                if (c[n].value == (c[n - 1].value + 1).toByte()) s.add(c[n])
                else {
                    if(s.size >= 5 ) {
                        val five = s.takeLast(5)
                        if (checkFlush(five) || !straightFlush) {
                            straight = five
                            straightFlush = checkFlush(straight)
                        }
                    }
                    s.clear()
                    s.add(c[n])
                }
            }
            if(s.isNotEmpty()){
                if(ace != null && s.last().value == 12.toByte()) s.add(ace)
                if(s.size >= 5 ) {
                    val five = s.takeLast(5)
                    if (checkFlush(five) || !straightFlush) {
                        straight = five
                        straightFlush = checkFlush(straight)
                    }
                }
            }

            val g = c.groupBy { it.suit }
            for(f in g.values) if(f.size >= 5) flush = f

            if(straightFlush) straight?.let { c.removeAll(it.toSet()) }

            val pairsArray = ArrayList<Byte>()
            for(card in c) {
                when(c.count { cc -> cc.value == card.value }) {
                    4 -> {
                        four = card.value
                    }
                    3 -> {
                        if(triple == null) triple = card.value
                        else if(triple!! < card.value) triple = card.value
                    }
                    2 -> {
                        if (!pairsArray.contains(card.value)) pairsArray.add(card.value)
                    }
                }
            }
            var num = c.size - 1
            var n = 0
            while( n  <= num) {
                if(c[n].value == four || c[n].value == triple) {
                    c.removeAt(n)
                    num--
                    n--
                } else for (p in pairsArray) {
                    if(c[n].value == p)  {
                        c.removeAt(n)
                        num--
                        n--
                        break
                    }
                }
                n++
            }

            pairs = if(pairsArray.size > 2) {
                pairsArray.sort()
                pairsArray.takeLast(2)
            } else pairsArray.toList()

            straight?.let { c.removeAll(it.toSet()) }
            flush?.let { c.removeAll(it.toSet()) }

            for(ca in c) if(ca.value == 0.toByte()) ca.value = 13.toByte()
            everythingElse = c.toList()
        }
    }

    private fun checkFlush(list : List<Card>?) : Boolean {
        list?.let {
            if(it.size > 1) {
                var amount = 0
                for(c in it) if(c.suit == it[0].suit) amount++
                if(amount >= 5 ) return true
            }
        }
        return false
    }

    fun compareTo(anotherCard : Hand) : Int {
        if(straightFlush || anotherCard.straightFlush) {
            if(straightFlush && anotherCard.straightFlush && !straight.isNullOrEmpty() && !anotherCard.straight.isNullOrEmpty()) return if(straight?.last()?.value!! > anotherCard.straight?.last()?.value!!) 1
            else if(straight?.last()?.value == anotherCard.straight?.last()?.value) 0
            else -1
            if(straightFlush && !anotherCard.straightFlush) return 1
            if(!straightFlush && anotherCard.straightFlush) return -1
        } else if(four != null || anotherCard.four != null) {
            if(four != null && anotherCard.four != null) return if(four!! > anotherCard.four!!) 1
            else if(four!! == anotherCard.four!!) tiebreaker(anotherCard)
            else -1
            if(four != null && anotherCard.four == null) return 1
            if(four == null && anotherCard.four != null) return -1
        } else if(triple != null && pairs.isNotEmpty() || anotherCard.triple != null && anotherCard.pairs.isNotEmpty()) {
            if(triple != null && pairs.isNotEmpty() && anotherCard.triple != null && anotherCard.pairs.isNotEmpty()) return if(triple!! > anotherCard.triple!!) 1
            else if(triple!! == anotherCard.triple!!) {
                val max = pairs.max()
                val anotherMax = anotherCard.pairs.max()
                if( max > anotherMax) 1
                else if(max == anotherMax) 0
                else -1
            } else -1
            if((triple != null && pairs.isNotEmpty()) && !(anotherCard.triple != null && anotherCard.pairs.isNotEmpty())) return 1
            if(!(triple != null && pairs.isNotEmpty()) && (anotherCard.triple != null && anotherCard.pairs.isNotEmpty())) return -1
        } else if(flush != null || anotherCard.flush != null) {
            if(flush != null && anotherCard.flush != null) return compareFlushes(flush!!, anotherCard.flush!!)
            if(flush != null && anotherCard.flush == null) return 1
            if(flush == null && anotherCard.flush != null) return -1
        } else if(straight != null || anotherCard.straight != null ) {
            if(straight != null && anotherCard.straight != null ) return if(straight?.last()?.value!! > anotherCard.straight?.last()?.value!!) 1
            else if(straight?.last()?.value == anotherCard.straight?.last()?.value) 0
            else -1
            if(straight != null && anotherCard.straight == null) return 1
            if(straight == null && anotherCard.straight != null) return -1
        } else if(triple != null || anotherCard.triple != null) {
            if(triple != null && anotherCard.triple != null) return if(triple!! > anotherCard.triple!!) 1
            else if(triple == anotherCard.triple) tiebreaker(anotherCard)
            else -1
            if(triple != null && anotherCard.triple == null) return 1
            if(triple == null && anotherCard.triple != null) return 1
        } else if(pairs.isNotEmpty() || anotherCard.pairs.isNotEmpty()) {
            if(pairs.size == anotherCard.pairs.size) {
                for(n in (pairs.size - 1) downTo 0) {
                    if(pairs[n] > anotherCard.pairs[n]) return 1
                    else if(pairs[n] < anotherCard.pairs[n]) return -1
                }
                return tiebreaker(anotherCard)
            } else return if(pairs.size > anotherCard.pairs.size) 1
            else -1
        } else {
            for(n in (everythingElse.size - 1) downTo 0) {
                if(everythingElse[n].value > anotherCard.everythingElse[n].value) return 1
                else if(everythingElse[n].value < anotherCard.everythingElse[n].value) return -1
            }
            return 0
        }
        return 0
    }

    private fun compareFlushes(first : List<Card>, second : List< Card>) : Int {
        if(first.size == 5 && second.size == 5) {
            for(n in 4 downTo 0) {
                if(first[n].value > second[n].value) return 1
                else if(first[n].value < second[n].value) return -1
            }
            return  0
        } else return  0
    }

    private fun tiebreaker(anotherCard : Hand) : Int {
        for(n in (everythingElse.size - 1) downTo 0) {
            if(everythingElse[n].value > anotherCard.everythingElse[n].value) return 1
            else if(everythingElse[n].value < anotherCard.everythingElse[n].value) return -1
        }
        return 0
    }
}