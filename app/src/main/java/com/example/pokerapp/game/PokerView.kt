package com.example.pokerapp.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.pokerapp.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.max
import kotlin.math.min
import kotlin.random.Random

class PokerView(context: Context, attributeSet: AttributeSet?) : View(context, attributeSet) {
    private var mWidth = 0
    private var mHeight = 0
    private var originalCardWidth = 79
    private var originalCardHeight = 123

    private val raiseAmount = 25

    private val signCheck = AppCompatResources.getDrawable(context,R.drawable.ic_baseline_equal)?.toBitmap() ?: Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)
    private val signFold = AppCompatResources.getDrawable(context,R.drawable.ic_baseline_remove)?.toBitmap() ?: Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)
    private val signRaise = AppCompatResources.getDrawable(context,R.drawable.ic_baseline_arrow_up)?.toBitmap() ?: Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)

    private val chip = BitmapFactory.decodeResource(context.resources, R.drawable.chip)
    private val restart = AppCompatResources.getDrawable(context,R.drawable.ic_baseline_replay)?.toBitmap() ?: Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)
    private val deck = BitmapFactory.decodeResource(context.resources, R.drawable.deck)
    //private val cardBack = Bitmap.createBitmap(deck, 79 * 4, 123 * 8, 79 * 2, 123 * 2)
    private var cardBack: Bitmap
    private val yellowFilter = LightingColorFilter(Color.YELLOW, 1)
    private var cardWidth = 1.0f
    private var cardHeight = 1.0f
    private var chipSize = 1.0f

    private var state = GAME
    private var bigBlindPlayer = 0

    private var playerChips = arrayListOf(500, 500, 500, 500)
    private var playerBets = arrayListOf(0, 0, 0, 0)


    private var takenCards = ArrayList<Pair<Suit, Byte>>()
    private var midCards = List(5) { pullCard() }
    private var playersCards = List(8) { pullCard() }

    private var winCardOpenedTimer = 0

    private var openedCards = 0

    private var moveTimer = 0
    private var currentPlayer = 0
    private var playersAction = arrayListOf(PlayerAction.WAITING, PlayerAction.WAITING, PlayerAction.WAITING, PlayerAction.WAITING) // 0 - nothing  1 - fold 2 - call/check 3 - raise

    private var winners = ArrayList<Int>()
    private var winnerNotificationTimer = 0

    companion object {
        const val GAME = 0
        const val WIN_SCREEN = 1
        const val ACTION = 2
        const val OPEN = 3
        const val ACTION_RAISE = 4
    }

    init {
        originalCardWidth = deck.width / 13
        originalCardHeight = deck.height / 5
        cardBack = Bitmap.createBitmap(deck, originalCardWidth * 2, originalCardHeight * 4, originalCardWidth, originalCardHeight)



        restart()
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            cardWidth = min(mWidth, mHeight) * 0.66f / 5.0f
            cardHeight = ((originalCardHeight / originalCardWidth.toFloat()) * cardWidth)
            chipSize = cardWidth * 0.8f
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            drawTable(it)
            drawUI(it)
            if(state == GAME) step()
            if(state == OPEN && winners.isEmpty()) checkWinner()
            if(state == OPEN && winners.isNotEmpty()) tickWinnerTimerNotification()
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                when(state) {
                    GAME -> {

                    }
                    WIN_SCREEN -> {

                    }
                }
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                when(state) {
                    GAME -> {

                    }
                    WIN_SCREEN -> {

                    }
                }
                return true
            }
            MotionEvent.ACTION_UP -> {
                when(state) {
                    GAME -> {

                    }
                    ACTION -> {
                        actionPress(event.x, event.y)
                    }
                    ACTION_RAISE -> {
                        actionPress(event.x, event.y)
                    }
                    WIN_SCREEN -> {
                        restart()
                    }
                    OPEN -> {
                        winCardOpenedTimer = 360
                    }
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    //Public Functions
    fun saveState() : GameState {
        return GameState(state, bigBlindPlayer, playerChips, playerBets, takenCards, midCards, playersCards, winCardOpenedTimer, openedCards, moveTimer, currentPlayer, playersAction, winners, winnerNotificationTimer)
    }

    fun loadState(gameState: GameState) {
        state = gameState.state
        bigBlindPlayer = gameState.bigBlindPlayer
        playerChips = gameState.playerChips
        playerBets = gameState.playerBets
        takenCards = gameState.takenCards
        midCards = gameState.midCards
        playersCards = gameState.playersCards
        winCardOpenedTimer = gameState.winCardOpenedTimer
        openedCards = gameState.openedCards
        moveTimer = gameState.moveTimer
        currentPlayer = gameState.currentPlayer
        playersAction = gameState.playersAction
        winners = gameState.winners
        winnerNotificationTimer = gameState.winnerNotificationTimer
    }


    //Private Functions

    private fun drawTable(c: Canvas) {
        val p = Paint()
        p.color = ResourcesCompat.getColor(context.resources, R.color.white, null)
        p.textAlign = Paint.Align.CENTER
        p.textSize = 50.0f
        p.color = Color.RED
        when(state) {
            OPEN -> {
                if(playersAction[1] != PlayerAction.LOST && playersAction[1] != PlayerAction.FOLD) {
                    for (n in 0..1) {
                        val l = (mWidth / 2.0f - n * cardWidth).toInt()
                        c.drawBitmap(playersCards[2 + n].bitmap,null,Rect( l, 0, l + cardWidth.toInt(), cardHeight.toInt()), p)
                        }
                }
                if(playersAction[0] != PlayerAction.LOST && playersAction[0] != PlayerAction.FOLD) {
                    for (n in 0..1) {
                        val top = (mHeight / 2 - n * cardWidth).toInt()
                        c.drawBitmap(playersCards[n].bitmap.rotate(90.0f),null, Rect( 0, top, cardHeight.toInt(), top + cardWidth.toInt()), p)
                    }
                }
                if(playersAction[2] != PlayerAction.LOST && playersAction[2] != PlayerAction.FOLD) {
                    for (n in 0..1) {
                        val top = (mHeight / 2 - n * cardWidth).toInt()
                        c.drawBitmap(playersCards[4 + n].bitmap.rotate(270.0f),null, Rect( mWidth - cardHeight.toInt(), top, mWidth, top + cardWidth.toInt()), p)
                    }
                }
                for (n in 0..1) {
                    val left = (mWidth / 2 - (n * cardWidth * 2)).toInt()
                    c.drawBitmap(playersCards[6 + n].bitmap, null, Rect( left, mHeight - cardHeight.toInt(), left + cardWidth.toInt() * 2, mHeight + cardHeight.toInt()), p)
                }
            }
            else -> {
                val card = cardBack.rotate(90.0f)
                if(playersAction[1] != PlayerAction.LOST) {
                    for (n in 0..1) {
                        val l = (mWidth / 2.0f - n * cardWidth).toInt()
                        c.drawBitmap(cardBack,null,Rect( l, 0, l + cardWidth.toInt(), cardHeight.toInt()), p)
                    }
                }
                if(playersAction[0] != PlayerAction.LOST) {
                    for (n in 0..1) {
                        val top = (mHeight / 2 - n * cardWidth).toInt()
                        c.drawBitmap(card,null, Rect( 0, top, cardHeight.toInt(), top + cardWidth.toInt()), p)
                    }
                }
                if(playersAction[2] != PlayerAction.LOST) {
                    for (n in 0..1) {
                        val top = (mHeight / 2 - n * cardWidth).toInt()
                        c.drawBitmap(card,null, Rect( mWidth - cardHeight.toInt(), top, mWidth, top + cardWidth.toInt()), p)
                    }
                }
                for (n in 0..1) {
                    val left = (mWidth / 2 - (n * cardWidth * 2)).toInt()
                    c.drawBitmap(playersCards[6 + n].bitmap, null, Rect( left, mHeight - cardHeight.toInt(), left + cardWidth.toInt() * 2, mHeight + cardHeight.toInt()), p)
                }
            }
        }
        p.textSize = 30.0f
        p.color = ResourcesCompat.getColor(context.resources, R.color.white, null)
        c.drawBitmap(chip, null, Rect( 0, (mHeight / 2 - cardWidth - 3 - chipSize).toInt(), chipSize.toInt(), (mHeight / 2  - cardWidth - 3).toInt()), p)
        c.drawText(playerChips[0].toString(), chipSize / 2, mHeight / 2 - cardWidth - 3 - chipSize / 2 + 10, p)
        c.drawBitmap(chip, null, Rect( (mWidth / 2 + cardWidth + 3).toInt(),0, (mWidth / 2 + cardWidth + 3 + chipSize).toInt(), chipSize.toInt()), p)
        c.drawText(playerChips[1].toString(), mWidth / 2 + cardWidth + 3 + chipSize / 2, chipSize / 2 + 10, p)
        c.drawBitmap(chip, null, Rect( (mWidth - chipSize).toInt(),(mHeight / 2 + cardWidth + 3).toInt(), mWidth, (mHeight / 2 + cardWidth + 3 + chipSize).toInt()), p)
        c.drawText(playerChips[2].toString(), mWidth - chipSize / 2, mHeight / 2 + cardWidth + 3 + chipSize / 2 + 10, p)
        c.drawBitmap(chip, null, Rect( (mWidth / 2 - cardWidth * 2 - 3 - chipSize).toInt(), (mHeight - chipSize).toInt(), (mWidth / 2 - cardWidth * 2 - 3).toInt(), mHeight ), p)
        c.drawText(playerChips[3].toString(), mWidth / 2 - cardWidth * 2 - 3 - chipSize / 2, mHeight - chipSize / 2 + 10, p)

        for(n in 0..4) {
            val l = (mWidth / 2 - (n * (cardWidth + 3)) / 2).toInt()
            c.drawBitmap(if(n < openedCards) midCards[n].bitmap else cardBack, null, Rect( l, (mHeight / 2 - cardHeight / 2).toInt(), l + cardWidth.toInt() + 3, (mHeight / 2 + cardHeight / 2).toInt()), p)
        }

        p.colorFilter = yellowFilter
        if(playersAction[0] != PlayerAction.LOST) {
            c.drawBitmap(chip, null, Rect((chipSize * 2.0f).toInt(), (mHeight / 2 - cardWidth - 3 - chipSize).toInt(), chipSize.toInt() * 3, (mHeight / 2  - cardWidth - 3).toInt()), p)
            c.drawText(playerBets[0].toString(), chipSize * 2.5f, mHeight / 2 - cardWidth - 3 - chipSize / 2 + 10, p)
        }
        if(playersAction[1] != PlayerAction.LOST) {
            c.drawBitmap(chip, null, Rect( (mWidth / 2 + cardWidth + 3).toInt(), (chipSize * 2.0f).toInt(), (mWidth / 2 + cardWidth + 3 + chipSize).toInt(), chipSize.toInt() * 3), p)
            c.drawText(playerBets[1].toString(), mWidth / 2 + cardWidth + 3 + chipSize / 2, chipSize * 2.5f + 10, p)
        }
        if(playersAction[2] != PlayerAction.LOST) {
            c.drawBitmap(chip, null, Rect( (mWidth - chipSize * 3).toInt(),(mHeight / 2 + cardWidth + 3).toInt(), (mWidth - chipSize * 2).toInt(), (mHeight / 2 + cardWidth + 3 + chipSize).toInt()), p)
            c.drawText(playerBets[2].toString(), mWidth - chipSize * 2.5f, mHeight / 2 + cardWidth + 3 + chipSize / 2 + 10, p)
        }

        val x : Float = if(mWidth > mHeight)  mWidth / 2.0f - cardWidth * 2.0f - 10 - chipSize * 3.5f
        else mWidth * 0.5f
        val y : Float = if(mWidth > mHeight)  mHeight - chipSize * 0.5f
        else  mHeight - cardHeight - 25 - chipSize * 0.5f - (min(mWidth, mHeight) * 0.75f - 15.0f) / 4.0f
        c.drawBitmap(chip, null, Rect( (x - chipSize / 2).toInt(), (y - chipSize / 2).toInt(), (x + chipSize / 2).toInt(), (y + chipSize / 2).toInt()), p)
        c.drawText(playerBets[3].toString(), x, y + 10, p)

        p.colorFilter = null
        p.color = Color.RED
        if(playersAction[0] == PlayerAction.ALL_IN) c.drawText("ВАБАНК",chipSize * 2.5f, mHeight / 2 - cardWidth - 3 - chipSize / 2 + 10 - 50, p )
        if(playersAction[1] == PlayerAction.ALL_IN)  c.drawText("ВАБАНК", mWidth / 2 + cardWidth + 3 + chipSize / 2, chipSize * 2.5f + 60, p)
        if(playersAction[2] == PlayerAction.ALL_IN) c.drawText("ВАБАНК", mWidth - chipSize * 2.5f, mHeight / 2 + cardWidth + 3 + chipSize / 2 + 10 - 50, p)
        if(playersAction[3] == PlayerAction.ALL_IN) c.drawText("ВАБАНК", x, y + 10 - 50, p)
    }

    private fun drawUI(c: Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        p.strokeWidth = 3.0f
        p.textSize = 20.0f
        p.textAlign = Paint.Align.CENTER
        p.color = ResourcesCompat.getColor(context.resources, R.color.grey, null)
        p.strokeWidth = 3.0f
        if(state != WIN_SCREEN) {
            if(playersAction[0] != PlayerAction.WAITING && playersAction[0] != PlayerAction.LOST) {
                val icon = when(playersAction[0]) {
                    PlayerAction.FOLD -> signFold
                    PlayerAction.RAISE -> signRaise
                    else -> signCheck
                }
                c.drawBitmap(icon, null, Rect(0,
                    (mHeight / 2 + cardWidth + 5).toInt(),
                    chipSize.toInt(), (mHeight / 2 + cardWidth + 5 + chipSize).toInt()), p)
            }
            if(playersAction[1] != PlayerAction.WAITING && playersAction[1] != PlayerAction.LOST) {
                val icon = when(playersAction[1]) {
                    PlayerAction.FOLD -> signFold
                    PlayerAction.RAISE -> signRaise
                    else -> signCheck
                }
                c.drawBitmap(icon, null, Rect(
                    (mWidth / 2 - cardWidth - 5 - chipSize).toInt(),
                    0, (mWidth / 2 - cardWidth - 5).toInt(), chipSize.toInt()), p)
            }
            if(playersAction[2] != PlayerAction.WAITING && playersAction[2] != PlayerAction.LOST) {
                val icon = when(playersAction[2]) {
                    PlayerAction.FOLD -> signFold
                    PlayerAction.RAISE -> signRaise
                    else -> signCheck
                }
                c.drawBitmap(icon, null, Rect(
                    (mWidth - chipSize).toInt(),
                    (mHeight / 2 - chipSize - cardWidth - 5).toInt(), mWidth, (mHeight / 2 - cardWidth - 5).toInt()), p)
            }
        }

        when(state) {
            GAME -> {

            }
            WIN_SCREEN -> {
                p.style = Paint.Style.FILL
                p.color = ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)
                c.drawRoundRect(mWidth / 4.0f,
                    mHeight * 0.25f,
                    mWidth * 0.75f, mHeight * 0.75f, 20f, 20f, p)
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey, null)
                p.style = Paint.Style.STROKE
                c.drawRoundRect(mWidth / 4.0f,
                    mHeight * 0.25f,
                    mWidth * 0.75f, mHeight * 0.75f, 20f, 20f, p)

                p.textSize = 40.0f
                p.style = Paint.Style.FILL
                p.color = Color.YELLOW
                c.drawText("Заново?", mWidth / 2.0f, mHeight * 0.3f, p)
                p.textSize = 30.0f
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey, null)
                //c.drawText("Вы обыграли всех противников", mWidth / 2.0f, mHeight * 0.3f + 30, p)

                val restartSize = min(mWidth, mHeight) * 0.3f

                c.drawBitmap(restart, null, Rect(((mWidth - restartSize) / 2).toInt(),
                    (mHeight * 0.75f - restartSize - 10).toInt(),
                    ((mWidth + restartSize) / 2).toInt(), (mHeight * 0.75f - 10).toInt()), p)
            }
            ACTION -> {
                val bSize = (min(mWidth, mHeight) * 0.75f - 15.0f) / 4.0f
                val left = mWidth / 2.0f - 7.5f - bSize * 2.0f
                val bottom = mHeight - cardHeight - 15

                p.style = Paint.Style.FILL
                p.color = ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)
                c.drawRoundRect(left - 10, bottom - bSize - 10, left  + bSize * 4 + 25, bottom + 10, 20.0f, 20.0f, p)
                p.color = ResourcesCompat.getColor(context.resources, R.color.gold, null)
                p.style = Paint.Style.STROKE
                c.drawRoundRect(left - 10, bottom - bSize - 10, left  + bSize * 4 + 25, bottom + 10, 20.0f, 20.0f, p)

                p.style = Paint.Style.FILL
                p.color = ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)
                c.drawRoundRect(left, bottom - bSize, left + bSize, bottom, 20.0f, 20.0f, p)
                p.color = ResourcesCompat.getColor(context.resources, R.color.gold, null)
                p.style = Paint.Style.STROKE
                c.drawRoundRect(left, bottom - bSize, left + bSize, bottom, 20.0f, 20.0f, p)
                c.drawBitmap(signCheck, null, Rect(left.toInt(), (bottom - bSize).toInt(), (left + bSize).toInt(), bottom.toInt()), p)
                c.drawText("пропустить", left + bSize / 2, bottom - 20, p)

                p.style = Paint.Style.FILL
                p.color = ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)
                c.drawRoundRect(left + bSize + 5, bottom - bSize, left  + bSize * 2 + 5, bottom, 20.0f, 20.0f, p)
                p.color = ResourcesCompat.getColor(context.resources, R.color.gold, null)
                p.style = Paint.Style.STROKE
                c.drawRoundRect(left + bSize + 5, bottom - bSize, left  + bSize * 2 + 5, bottom, 20.0f, 20.0f, p)
                c.drawBitmap(signCheck, null, Rect((left + bSize + 5).toInt(), (bottom - bSize).toInt(), (left  + bSize * 2 + 5).toInt(), bottom.toInt()), p)
                c.drawText("уравнять", left + 5 + bSize * 1.5f, bottom - 20, p)

                p.style = Paint.Style.FILL
                p.color = ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)
                c.drawRoundRect(left + bSize * 2 + 10, bottom - bSize, left  + bSize * 3 + 10, bottom, 20.0f, 20.0f, p)
                p.color = ResourcesCompat.getColor(context.resources, R.color.gold, null)
                p.style = Paint.Style.STROKE
                c.drawRoundRect(left + bSize * 2 + 10, bottom - bSize, left  + bSize * 3 + 10, bottom, 20.0f, 20.0f, p)
                c.drawBitmap(signRaise, null, Rect((left + bSize * 2 + 10).toInt(), (bottom - bSize).toInt(), (left  + bSize * 3 + 10).toInt(), bottom.toInt()), p)
                c.drawText("повысить", left + 10 + bSize * 2.5f, bottom - 20, p)

                p.style = Paint.Style.FILL
                p.color = ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)
                c.drawRoundRect(left + bSize * 3 + 15, bottom - bSize, left  + bSize * 4 + 15, bottom, 20.0f, 20.0f, p)
                p.color = ResourcesCompat.getColor(context.resources, R.color.gold, null)
                p.style = Paint.Style.STROKE
                c.drawRoundRect(left + bSize * 3 + 15, bottom - bSize, left  + bSize * 4 + 15, bottom, 20.0f, 20.0f, p)
                c.drawBitmap(signFold, null, Rect((left + bSize * 3 + 15).toInt(), (bottom - bSize).toInt(), (left  + bSize * 4 + 15).toInt(), bottom.toInt()), p)
                c.drawText("сдаться", left + 15 + bSize * 3.5f, bottom - 20, p)
            }
            ACTION_RAISE -> {
                val bSize = (min(mWidth, mHeight) * 0.75f - 15.0f) / 4.0f
                val left = (mWidth - 7.5f - bSize * 4.0f) / 2.0f
                val bottom = mHeight - cardHeight - 15
                p.textSize = 50.0f

                p.style = Paint.Style.FILL
                p.color = ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)
                c.drawRoundRect(left - 10, bottom - bSize - 10, left  + bSize * 4 + 25, bottom + 10, 20.0f, 20.0f, p)
                p.color = ResourcesCompat.getColor(context.resources, R.color.gold, null)
                p.style = Paint.Style.STROKE
                c.drawRoundRect(left - 10, bottom - bSize - 10, left  + bSize * 4 + 25, bottom + 10, 20.0f, 20.0f, p)

                p.style = Paint.Style.FILL
                p.color = ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)
                c.drawRoundRect(left, bottom - bSize, left + bSize, bottom, 20.0f, 20.0f, p)
                p.color = ResourcesCompat.getColor(context.resources, R.color.gold, null)
                p.style = Paint.Style.STROKE
                c.drawRoundRect(left, bottom - bSize, left + bSize, bottom, 20.0f, 20.0f, p)
                p.style = Paint.Style.FILL
                c.drawText("$raiseAmount", left + bSize / 2, bottom - bSize * 0.4f, p)

                p.style = Paint.Style.FILL
                p.color = ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)
                c.drawRoundRect(left + bSize + 5, bottom - bSize, left  + bSize * 2 + 5, bottom, 20.0f, 20.0f, p)
                p.color = ResourcesCompat.getColor(context.resources, R.color.gold, null)
                p.style = Paint.Style.STROKE
                c.drawRoundRect(left + bSize + 5, bottom - bSize, left  + bSize * 2 + 5, bottom, 20.0f, 20.0f, p)
                p.style = Paint.Style.FILL
                c.drawText("${raiseAmount * 2}", left + 5 + bSize * 1.5f, bottom - bSize * 0.4f, p)

                p.textSize = 25.0f
                p.style = Paint.Style.FILL
                p.color = ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)
                c.drawRoundRect(left + bSize * 2 + 10, bottom - bSize, left  + bSize * 3 + 10, bottom, 20.0f, 20.0f, p)
                p.color = ResourcesCompat.getColor(context.resources, R.color.gold, null)
                p.style = Paint.Style.STROKE
                c.drawRoundRect(left + bSize * 2 + 10, bottom - bSize, left  + bSize * 3 + 10, bottom, 20.0f, 20.0f, p)
                p.style = Paint.Style.FILL
                c.drawText("Вабанк", left + 5 + bSize * 2.5f, bottom - bSize * 0.4f, p)

                p.style = Paint.Style.FILL
                p.color = ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)
                c.drawRoundRect(left + bSize * 3 + 15, bottom - bSize, left  + bSize * 4 + 15, bottom, 20.0f, 20.0f, p)
                p.color = ResourcesCompat.getColor(context.resources, R.color.gold, null)
                p.style = Paint.Style.STROKE
                c.drawRoundRect(left + bSize * 3 + 15, bottom - bSize, left  + bSize * 4 + 15, bottom, 20.0f, 20.0f, p)
                p.style = Paint.Style.FILL
                c.drawText("отмена", left + 5 + bSize * 3.5f, bottom - bSize * 0.4f, p)
            }
            OPEN -> {
                p.textSize = 25.0f
                val w = 150.0f
                val h = 50.0f
                var x = 0.0f
                var y = 0.0f
                for(winner in winners)  {
                    when(winner) {
                        0 -> {
                            x = w / 2 + 10.0f
                            y = mHeight / 2.0f
                        }
                        1 -> {
                            x = mWidth / 2.0f
                            y = h / 2 + 10
                        }
                        2 -> {
                            x = mWidth - w / 2.0f - 10
                            y = mHeight / 2.0f
                        }
                        3 -> {
                            x = mWidth  / 2.0f
                            y = mHeight - h / 2.0f - 10
                        }
                    }
                    p.style = Paint.Style.FILL
                    p.color = ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)
                    c.drawRoundRect(x - w / 2, y - h / 2 , x + w / 2, y + h / 2, 10.0f, 10.0f, p)
                    p.color = ResourcesCompat.getColor(context.resources, R.color.gold, null)
                    p.style = Paint.Style.STROKE
                    c.drawRoundRect(x - w / 2, y - h / 2 , x + w / 2, y + h / 2, 10.0f, 10.0f, p)
                    p.style = Paint.Style.FILL
                    c.drawText("Победитель", x, y + 10, p)
                }
            }
        }
    }

    private fun step()  {
        var moveUnfinished = false
        var maxBet = 0
        var players = 4
        for(n in 0..3) {
            if(playersAction[n] != PlayerAction.FOLD && maxBet < playerBets[n]) maxBet = playerBets[n]
            if(playersAction[n] == PlayerAction.LOST || playersAction[n] == PlayerAction.FOLD || playersAction[n] == PlayerAction.ALL_IN) players--
        }

        for(n in 0..3) {
            val p = (currentPlayer + n) % 4
            if(playersAction[p] == PlayerAction.WAITING || (playerBets[p] < maxBet && playersAction[p] != PlayerAction.FOLD && playersAction[p] != PlayerAction.ALL_IN && playersAction[p] != PlayerAction.LOST)) {
                moveUnfinished = true
                currentPlayer = p
                break
            }
        }
        if(moveUnfinished) {
            if(currentPlayer == 3) state = ACTION
            else aiMove()
        } else {
            if(players < 2) openedCards = 5
            if(openedCards == 5) {
                checkWinner()
              } else {
                currentPlayer = bigBlindPlayer + 1
                openedCards++
                for(n in 0..3) if(playersAction[n] != PlayerAction.FOLD && playersAction[n] != PlayerAction.ALL_IN && playersAction[n] != PlayerAction.LOST) playersAction[n] = PlayerAction.WAITING
            }
        }
    }

    private fun aiMove() {
        moveTimer++
        if (moveTimer > 60) {
            var dif = 0
            for(n in 0..3) dif = max(dif, playerBets[n] - playerBets[currentPlayer])

            val choice = arrayListOf(PlayerAction.FOLD, PlayerAction.ALL_IN)
            if(dif <= playerChips[currentPlayer]) choice.add(PlayerAction.CALL)
            if(playerChips[currentPlayer] >= dif + raiseAmount) choice.add(PlayerAction.RAISE)

            val raisePossibility = if(choice.contains(PlayerAction.RAISE)) 25 else 0
            val callPossibility = if(choice.contains(PlayerAction.CALL)) 45 + 25 - raisePossibility else 0
            val foldPossibility = (100 - raisePossibility - callPossibility) / 3 * 2
            playersAction[currentPlayer] = when( Random.nextInt(100) ) {
                in 0..callPossibility -> PlayerAction.CALL
                in callPossibility..callPossibility + raisePossibility -> PlayerAction.RAISE
                in callPossibility + raisePossibility..callPossibility + raisePossibility + foldPossibility -> PlayerAction.FOLD
                else -> PlayerAction.ALL_IN
            }
            //playersAction[currentPlayer] = choice.random()
            moveTimer = 0

            when(playersAction[currentPlayer]) {
                PlayerAction.CALL -> {
                    playerBets[currentPlayer] += dif
                    playerChips[currentPlayer] -= dif
                    if(playerChips[currentPlayer] == 0 ) playersAction[currentPlayer] = PlayerAction.ALL_IN
                }
                PlayerAction.RAISE -> {
                    playerBets[currentPlayer] += dif + raiseAmount
                    playerChips[currentPlayer] -= dif + raiseAmount
                    if(playerChips[currentPlayer] == 0 ) playersAction[currentPlayer] = PlayerAction.ALL_IN
                }
                PlayerAction.ALL_IN -> {
                    playerBets[currentPlayer] += playerChips[currentPlayer]
                    playerChips[currentPlayer] = 0
                }
                else -> {}
            }
            currentPlayer = (currentPlayer + 1) % 4
        }
    }

    private fun pullCard() : Card {
        val freeCards = ArrayList<Pair<Suit, Byte>>()
        for(s in Suit.values()) {
            for(n in 0 until 13) if(!takenCards.contains(Pair(s, n.toByte()))) freeCards.add(Pair(s, n.toByte()))
        }
        val random = freeCards.random()
        val b = Bitmap.createBitmap(deck, originalCardWidth * random.second, originalCardHeight * random.first.n, originalCardWidth, originalCardHeight)
        return Card(random.first, random.second, b)
    }

    private fun Bitmap.rotate(degree : Float) : Bitmap {
        val transformMatrix = Matrix()
        transformMatrix.postRotate(degree)
       return Bitmap.createBitmap(this, 0,0, width, height, transformMatrix, true)
    }

    private fun actionPress(x: Float, y: Float) {
        val bSize = (min(mWidth, mHeight) * 0.75f - 15.0f) / 4.0f
        val left = mWidth / 2.0f - 7.5f - bSize * 2.0f
        val bottom = mHeight - cardHeight - 5


        val firstButton = Rect((left - 10).toInt(), (bottom - bSize - 10).toInt(), (left  + bSize).toInt(),
            (bottom + 10).toInt())
        val secondButton = Rect(
            (left + bSize + 5).toInt(), (bottom - bSize).toInt(), (left  + bSize * 2 + 5).toInt(),
            bottom.toInt())
        val thirdButton = Rect(
            (left + bSize * 2 + 10).toInt(), (bottom - bSize).toInt(),
            (left  + bSize * 3 + 10).toInt(),
            bottom.toInt())
        val forthButton = Rect(
            (left + bSize * 3 + 15).toInt(), (bottom - bSize).toInt(),
            (left  + bSize * 4 + 15).toInt(), bottom.toInt())

        when(state) {
            ACTION -> {
                var dif = 0
                for(i in 0..3) if(playerBets[i] > dif) dif = playerBets[i]
                dif -= playerBets[3]

                if(firstButton.contains(x.toInt(), y.toInt())) {
                    playersAction[3] = PlayerAction.CHECK
                    state = GAME
                } else if(secondButton.contains(x.toInt(), y.toInt()) && playerChips[3] >= dif) {
                    playerChips[3] -= dif
                    playerBets[3] += dif
                    playersAction[3] = PlayerAction.CALL
                    currentPlayer = 0
                    state = GAME
                    if(playerChips[3] == 0) playersAction[3] = PlayerAction.ALL_IN
                } else if(thirdButton.contains(x.toInt(), y.toInt())) {
                    state = ACTION_RAISE
                } else if(forthButton.contains(x.toInt(), y.toInt())) {
                    playersAction[3] = PlayerAction.FOLD
                    currentPlayer = 0
                    state = GAME
                }
            }
            ACTION_RAISE -> {
                var diff = 0
                for(n in 0..2) diff = max(diff, playerBets[n] - playerBets[3])
                if(firstButton.contains(x.toInt(), y.toInt()) && playerChips[3] >= raiseAmount) {
                    playersAction[3] = PlayerAction.RAISE
                    playerChips[3] -= diff + raiseAmount
                    playerBets[3] += diff + raiseAmount
                    state = GAME
                    currentPlayer = 0
                    if(playerChips[3] == 0) playersAction[3] = PlayerAction.ALL_IN
                } else if(secondButton.contains(x.toInt(), y.toInt()) && playerChips[3] >= raiseAmount * 2) {
                    playerChips[3] -= diff + raiseAmount * 2
                    playerBets[3] += diff + raiseAmount * 2
                    playersAction[3] = PlayerAction.RAISE
                    currentPlayer = 0
                    state = GAME
                    if(playerChips[3] == 0) playersAction[3] = PlayerAction.ALL_IN
                } else if(thirdButton.contains(x.toInt(), y.toInt())) {
                    playerBets[3] += playerChips[3]
                    playerChips[3] = 0
                    playersAction[3] = PlayerAction.ALL_IN
                    currentPlayer = 0
                    state = GAME
                } else if(forthButton.contains(x.toInt(), y.toInt())) {
                    state = ACTION
                }
            }
        }
    }

    private fun checkWinner() {
        var players = 4
        for(n in 0..3) if(playersAction[n] == PlayerAction.LOST || playersAction[n] == PlayerAction.FOLD) players--
        if(players >= 2) {
            state = OPEN
            winCardOpenedTimer++
            if(winCardOpenedTimer > 360) {
                val hands = listOf(Hand(midCards.plus(playersCards[0]).plus(playersCards[1])),
                    Hand(midCards.plus(playersCards[2]).plus(playersCards[3])),
                    Hand(midCards.plus(playersCards[4]).plus(playersCards[5])),
                    Hand(midCards.plus(playersCards[6]).plus(playersCards[7])))

                for(n in 0..3) {
                    if(playersAction[n] != PlayerAction.LOST && playersAction[n] != PlayerAction.FOLD) {
                        if(winners.isEmpty()) winners.add(n)
                        else if(hands[n].compareTo(hands[winners[0]]) > 0) {
                            winners.clear()
                            winners.add(n)
                        } else if(hands[n].compareTo(hands[winners[0]]) == 0) winners.add(n)
                    }
                }
                val pot = playerBets.sum()
                for(w in winners) playerChips[w] += pot / winners.size
                if(playerChips[3] <= 0) state = WIN_SCREEN
            }
        } else {
            var winner = 0
            for(n in 0..3) {
                if(playersAction[n] != PlayerAction.LOST && playersAction[n] != PlayerAction.FOLD) winner = n
            }
            playerChips[winner] += playerBets.sum()
            if(playerChips[3] <= 0) state = WIN_SCREEN
            newCircle()
            winCardOpenedTimer = 0
        }
    }

    private fun tickWinnerTimerNotification() {
        winnerNotificationTimer++
        if(winnerNotificationTimer > 40) {
            winners.clear()
            newCircle()
            winCardOpenedTimer = 0
            winnerNotificationTimer = 0
        }
    }

    private fun newCircle() {
        if(takenCards.size > 39 ) takenCards.clear()
        var gameContinues = false
        var smB = 0
        for(n in 0..3) if(playerChips[n] == 0) playersAction[n] = PlayerAction.LOST

        for(n in 1..4) {
            val p = (bigBlindPlayer + n) % 4
            if(playerChips[p] > raiseAmount * 2) {
                bigBlindPlayer = p
                for(nn in 1..3) {
                    val sBP = (p + nn) % 4
                    if(playerChips[sBP] > raiseAmount ) {
                        smB = sBP
                        gameContinues = true
                        break
                    } else playersAction[sBP] = PlayerAction.LOST
                }
                break
            } else playersAction[p] = PlayerAction.LOST
        }
        if(gameContinues) {
            for(n in 0..3) {
                if(playersAction[n] != PlayerAction.LOST) playersAction[n] = PlayerAction.WAITING
                playerBets[n] = 0
            }
            playerChips[bigBlindPlayer] -= raiseAmount * 2
            playerBets[bigBlindPlayer] += raiseAmount * 2
            playerChips[smB] -= raiseAmount
            playerBets[smB] += raiseAmount
            dealCards()
            currentPlayer = smB
            openedCards = 0
            state = GAME
        } else {
            state = WIN_SCREEN
        }
    }

    private fun dealCards() {
        midCards = List(5) { pullCard() }
        playersCards =  List(8) { pullCard() }
    }

    private fun restart() {
        for(n in 0..3) {
            playerChips[n] = 500
            playerBets[n] = 0
            playersAction[n] = PlayerAction.WAITING
        }
        newCircle()
        state = GAME
    }
}