package com.example.pokerapp.game

data class GameState( var state : Int,
                      val bigBlindPlayer : Int,
                      val playerChips : ArrayList<Int>,
                      val playerBets : ArrayList<Int>,
                      val takenCards : ArrayList<Pair<Suit, Byte>>,
                      val midCards : List<Card>,
                      val playersCards : List<Card>,
                      val winCardOpenedTimer : Int,
                      val openedCards : Int,
                      val moveTimer : Int,
                      val currentPlayer : Int,
                      val playersAction : ArrayList<PlayerAction>,
                      val winners : ArrayList<Int>,
                      var winnerNotificationTimer : Int)
